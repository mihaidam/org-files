#+TITLE:Declarație pe propria răspundere
#+LATEX_CLASS: article
#+OPTIONS: author:nil toc:nil
#+BEGIN_CENTER
Subsemnatul, DAMIAN Mihai, student la Universitatea de Vest din Timișoara,
Facultatea de Matematică și Informatică, specializarea Informatică, an 2, cu numărul matricol I2825, declar că
nu am efectuat stagiul de practică în mod formal și că nu am copiat sau dat să fie copiat dosarul de practică.
#+END_CENTER
