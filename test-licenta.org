%#+LATEX_COMPILER: xelatex
#+LATEX_CLASS: myreport
#+LATEX_CLASS_OPTIONS: [12pt,a4paper]
#+LATEX_HEADER: \usepackage{amsmath,amsthm,amssymb,graphicx,hyperref}
#+LATEX_HEADER: \usepackage[left=1.2in,right=1in,top=1in,bottom=1in]{geometry}
#+LATEX_HEADER: \usepackage[romanian]{babel}
#+LATEX_HEADER: %\usepackage{...insert other packages here...}
#+LATEX_HEADER: \newtheorem{thm}{Teorema}[section]
#+LATEX_HEADER: \newtheorem{lem}[thm]{Lema}
#+LATEX_HEADER: \newtheorem{cor}[thm]{Corolarul}
#+LATEX_HEADER: \newtheorem{prop}[thm]{Propozi\c tia}
#+LATEX_HEADER: \theoremstyle{definition}
#+LATEX_HEADER: \newtheorem{defn}{Defini\c tia}[section]
#+LATEX_HEADER: \theoremstyle{remark}
#+LATEX_HEADER: \newtheorem{rem}{Remarca}[section]
#+LATEX_HEADER: \newtheorem{exmp}{Exemplul}[section]

#+OPTIONS: toc:nil

bibliography:mybib.bib

\section*{Abstract}
rezumatul in limba engleza
\tableofcontents

\newpage
\section*{Introducere}
\addcontentsline{toc}{section}{Introducere}

introducere
* test 11111
** Titlul capitolului 1
*** Titlul secțiunii 1.1
 \begin{defn} ...sdaklfjklksjlkj cite:Mathelin_2018
 \end{defn}
 \begin{thm} ...
 \end{thm}
 \begin{proof} ...
 \end{proof}
 \begin{cor} ...
 \end{cor}
 \begin{prop} ...
 \end{prop}
 \begin{exmp} ...
 \end{exmp}

 \section{Titlul sec\c tiunii 1.2}

bibliographystyle:alpha
